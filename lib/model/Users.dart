class Users {
  String name;
  String descr;
  String price;
  String pic;
  String rating;
  String numOfReviews;
  String ETA;

  Users({this.name,
    this.descr,
    this.price,
    this.pic,
    this.rating,
    this.numOfReviews,
    this.ETA});

  factory Users.fromJson(Map<String, dynamic> json)
  {
    return Users(
      name: json['name'],
      descr: json['descr'],
      price: json['price'],
      pic: json['pic'],
      rating: json['rating'],
      numOfReviews: json['numOfReviews'],
      ETA: json['ETA'],
    );
  }
}

