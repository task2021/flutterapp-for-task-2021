import 'package:flutter/foundation.dart';
import 'package:flutter_app228/model/Users.dart';

class Cart extends ChangeNotifier {

  List<Users> _foods = [];
  int count = 0;

  Cart();

  void add(Users food) {
    _foods.add(food);
    update();
  }

  void remove(Users food) {
    _foods.remove(food);
    update();
  }

  update() {
    count = _foods.length;
    notifyListeners();
  }
  //
  // double get totalPrice {
  //   double total = 0;
  //   _foods.map((e) => total += e.price);
  //   return total;
  // }
}