import 'package:flutter/material.dart';
import 'package:flutter_app228/Screens/MainScreen.dart';
import 'package:flutter_app228/model/Cart.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => Cart(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
          accentColor: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: ChangeNotifierProvider(
          create: (_) => Cart(),
          child: MainScreen(),
        ),
      ),
    );
  }
}
