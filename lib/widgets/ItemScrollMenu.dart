import 'package:flutter_app228/Screens/LookupScreen.dart';
import 'package:flutter_app228/model/Cart.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_app228/model/Users.dart';
import 'dart:convert';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class ItemScrollMenu extends StatefulWidget {
  @override
  ItemScrollMenuState createState() => ItemScrollMenuState();
}

class ItemScrollMenuState extends State<ItemScrollMenu> {
  List<Users> _food = [];
  Users _food1;

  @override
  void initState() {
    // TODO: implement initState
    _getRequest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // return Scaffold(
    //   appBar: AppBar(
    //     title: Text("Hz"),
    //   ),
    //   body:
    return SizedBox(
      child: ListView.builder(
          itemCount: _food.length,
          itemBuilder: (context, index) {
            return Container(
              color: Color.fromARGB(0xff, 0xf2, 0xf2, 0xf2),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 165,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(_food[index].pic),
                          fit: BoxFit.fitWidth),
                    ),
                  ),
                  Padding(
                      child: Container(
                          child: Column(
                            children: [
                              Container(
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 185,
                                        child: Column(
                                          children: [
                                            Container(
                                                child: Text(_food[index].name,
                                                    style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .bold,
                                                    ))),
                                            Container(
                                                alignment: Alignment.centerLeft,
                                                height: 20,
                                                child: Row(
                                                  children: [
                                                    Container(
                                                        alignment: Alignment
                                                            .center,
                                                        width: 14,
                                                        height: 14,
                                                        child: SvgPicture.asset(
                                                            'assets/star.svg')),
                                                    Container(
                                                        padding:
                                                        EdgeInsets.only(
                                                            left: 7),
                                                        child: Center(
                                                            child: Text(
                                                                _food[index]
                                                                    .rating,
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold)))),
                                                    Container(
                                                        padding:
                                                        EdgeInsets.only(
                                                            left: 10),
                                                        child: Center(
                                                            child: Text(
                                                                _food[index]
                                                                    .numOfReviews,
                                                                style: TextStyle(
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .w300)))),
                                                    Container(
                                                        padding:
                                                        EdgeInsets.only(
                                                            left: 15),
                                                        child: Center(
                                                            child: Text(
                                                                _food[index]
                                                                    .ETA))),
                                                  ],
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                                ))
                                          ],
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,
                                        ),
                                      ),
                                      Expanded(
                                          child: Container(
                                            alignment: Alignment.bottomRight,
                                            height: 45,
                                            child: Text.rich(
                                                TextSpan(children: [
                                                  TextSpan(
                                                    text: '₽',
                                                  ),
                                                  TextSpan(
                                                      text: _food[index].price,
                                                      style: TextStyle(
                                                          color: Color.fromARGB(
                                                              0xff, 0x33, 0x33,
                                                              0x33),
                                                          fontSize: 32,
                                                          fontWeight: FontWeight
                                                              .bold))
                                                ])),
                                          ))
                                    ],
                                  )),
                              SizedBox(height: 10, width: 50,),
                              FloatingActionButton.extended(
                                onPressed: () {
                                  _addItemToCart(context, _food1);
                                  Navigator.of(context).push(
                                      PageRouteBuilder(
                                          transitionDuration: Duration(
                                              milliseconds: 500),
                                          pageBuilder: (_, anim, secAnim) =>
                                              LookupScreen(_food1),
                                          transitionsBuilder: (_, anim, secAnim,
                                              child) {
                                            var offsetAnimation = anim.drive(
                                                Tween(begin: Offset(1, 0),
                                                    end: Offset.zero));
                                            return SlideTransition(
                                              child: child,
                                              position: offsetAnimation,
                                            );
                                          }
                                      )
                                  );
                                },
                                tooltip: "Нажатие переключит на страницу продукта",
                                label: Text('Добавить в корзину'),
                                icon: Icon(Icons.thumb_up),
                                backgroundColor: Color.fromARGB(
                                    0xff, 0xf2, 0xc9, 0x4c),
                              ),
                            ],
                          )),
                      padding: EdgeInsets.only(
                          left: 45, right: 45, bottom: 20, top: 20))
                ],
              ),
            );
          }
      ),
    );
  }

  void _addItemToCart(BuildContext context, Users _food) {
    context.read<Cart>().add(_food);
    Scaffold.of(context).showSnackBar(
        SnackBar(content: Text('Заказ добавлен в корзину')));
  }

  Future<Users> _getRequest() async {
    http.Response response =
    await http.get(Uri.http('192.168.43.248:3000', ''));
    var rb = response.body;
    var list = json.decode(rb) as List;
    List<Users> listvalues = list.map((e) => Users.fromJson(e)).toList();

    setState(() {
      _food.addAll(listvalues);
    });
  }
}

