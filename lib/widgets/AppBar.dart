import 'package:flutter/material.dart';
import 'package:flutter_app228/model/Cart.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class CartButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    String val = context.watch<Cart>().count.toString();

    return Container(
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          SvgPicture.asset('assets/shoppingCart.svg'),
          ClipOval(
            child: Container(
              child: SizedBox(
                  width: 16,
                  height: 16,
                  child: Center(
                      child: Text(val , style: TextStyle(fontSize: 10), overflow: TextOverflow.visible)
                  )
              ),
              color: Color.fromARGB(0xff, 0xf2, 0xc9, 0x4c),
            ),
          )
        ],
        alignment: AlignmentDirectional.bottomStart,
      ),
    );
  }
}
